<?php namespace MuVO\KaInternet;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Uri;
use Monolog\Logger;
use Psr\Http\Message\UriInterface;
use Psr\Log\LoggerInterface;

class Hub
{
    /**
     * @var UriInterface
     */
    private $url;

    /**
     * @var LoggerInterface
     */
    public $logger;

    /**
     * @var string
     */
    private $login;

    /**
     * @var string
     */
    private $password;

    /**
     * @var Client
     */
    private $_client;

    public function __construct(string $login, string $password = null, array $options = [])
    {
        $this->logger = new Logger($login);
        $this->_client = new Client(($options['httpOptions'] ?? [])
            + ['verify' => false]);

        $this->url = new Uri($options['url'] ?? 'https://93.95.196.244/kai2hug.cgi');

        $this->login = $login;
        $this->password = $password;
    }

    /**
     * @param string $command
     * @param array|null $params
     * @return array
     */
    private function getBody(string $command, array $params = null)
    {
        $body = [
            'cmd' => $command,
            'auth' => [
                'lgn' => $this->login,
                'pwd' => $this->password,
            ]
        ];

        if (!is_null($params)) {
            $body['param'] = $params;
        }

        return $body;
    }

    /**
     * @param string $method
     * @param string $command
     * @param array|null $params
     * @return mixed|null
     */
    private function call(string $method, string $command, array $params = null)
    {
        $request = new Request(strtoupper($method), $this->url);

        $body = $this->getBody($command, $params);
        $this->logger->debug(json_encode($body));

        if ($result = $this->_client->send($request, ['json' => $body])) {
            $body = (string)$result->getBody();
            $this->logger->info($body);

            return \GuzzleHttp\json_decode($body);
        }

        return null;
    }

    /**
     * @param string $command
     * @param string $siteID
     * @return \stdClass|null
     */
    private function _query(string $command, string $siteID, string $httpMethod = 'POST')
    {
        if ($result = $this->call($httpMethod, $command, ['siteid' => $siteID])) {
            if (isset($result->status)) {
                $this->logger->notice(sprintf("Code: %s", $result->status));
                if (!in_array(intval($result->status), [200, 201])) {
                    throw new \ErrorException($result->message ?? "Empty response from remote API", $result->status);
                }
            }

            return $result;
        }

        return null;
    }

    public function create(string $sitePrefix, string $servicePlan)
    {
        return $this->call('post', 'create',
            [
                'serv' => $servicePlan,
                'siteid' => $sitePrefix,
            ]);
    }

    /**
     * @return string[]|null
     */
    public function fetch()
    {
        return $this->call('post', 'fetch');
    }

    /**
     * @param string $siteID
     * @return null|\stdClass
     */
    public function info(string $siteID)
    {
        return $this->_query('info', $siteID);
    }

    /**
     * @param string $siteID
     * @return null|\stdClass
     */
    public function param(string $siteID)
    {
        return $this->_query('param', $siteID);
    }

    /**
     * @param string $siteID
     * @return null|\stdClass
     */
    public function status(string $siteID)
    {
        return $this->_query('status', $siteID);
    }

    /**
     * @param string $siteID
     * @return null|\stdClass
     */
    public function reset(string $siteID)
    {
        return $this->_query('reset', $siteID);
    }

    /**
     * @param string $siteID
     * @return null|\stdClass
     */
    public function deactivate(string $siteID)
    {
        return $this->_query('deactivate', $siteID);
    }

    /**
     * @param string $siteID
     * @return null|\stdClass
     */
    public function delete(string $siteID)
    {
        return $this->_query('delete', $siteID);
    }


    /**
     * @param string $siteID
     * @return null|\stdClass
     */
    public function moveAllow(string $siteID)
    {
        return $this->_query('move_allow', $siteID, 'post');
    }

    /**
     * @param string $siteID
     * @return null|\stdClass
     */
    public function moveDisallow(string $siteID)
    {
        return $this->_query('move_disallow', $siteID, 'post');
    }

    /**
     * @param string $siteID
     * @return null|\stdClass
     */
    public function swapAllow(string $siteID)
    {
        return $this->_query('swap', $siteID, 'post');
    }

    /**
     * @param string $siteID
     * @return null|\stdClass
     */
    public function swapDisallow(string $siteID)
    {
        return $this->_query('swap_disallow', $siteID, 'post');
    }
}
